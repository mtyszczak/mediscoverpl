#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# mediscover - init script

"""
Social Media Discover (PL) lets you discover social media profiles by given username
"""

name =  'mediscover.py'
__author__ = 'LyingCarpet'
__version__ = '0.1.0'
__status__ = 'Development'


from modules.args import Arguments

args = Arguments()

if __name__ == '__main__':
    from modules import main as mediscover
    mediscover.run()
