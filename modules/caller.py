#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# caller.py - caller methods for parsed json

import json, requests, random, string
from mediscover import args
from requests import Request, Session


requests.packages.urllib3.disable_warnings()


def url_special_replace(url):
    url = url.replace("%%%USERNAME%%%", args.user)
    return url


def arg_special_replace(arg):
    arg = arg.replace("%%%USERNAME%%%", args.user)
    return arg


def post_data_contains(url, actionarg, action = "", headers = [], negate = False):
    url = url_special_replace(url)

    jsondata = actionarg[0]
    actionarg.pop(0)

    parseddt = {}

    for d in jsondata["values"]:
        parseddt[str(d["name"])] = arg_special_replace(str(d["value"]))

    if action == "":
        action = "or"

    pheaders = { "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36" }
    for h in headers:
        pheaders[h["name"]] = h["value"]

    try:
        r = requests.post(url, data=parseddt, headers=pheaders, allow_redirects=True)
    except requests.exceptions.SSLError:
        r = requests.post(url, data=parseddt, headers=pheaders, allow_redirects=True, verify=False)

    algood = False

    for arg in actionarg:
        arg = arg_special_replace(str(arg))
        if action == "or":
            if negate:
                if not arg in r.text:
                    return [ url ]
            else:
                if arg in r.text:
                    return [ url ]
        elif action == "and":
            if negate:
                if not arg in r.text:
                    algood = True
                else:
                    algood = False
            else:
                if arg in r.text:
                    algood = True
                else:
                    algood = False

    if algood:
        return [ url ]

    return []


def json_call_contains(url, actionarg, requestm = "", action = "", headers = [], negate = False):
    url = url_special_replace(url)

    jsondata = json.loads(arg_special_replace(str(actionarg[0])))
    actionarg.pop(0)

    if requestm == "":
        requestm = "post"

    if action == "":
        action = "or"

    pheaders = { "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36" }
    for h in headers:
        pheaders[h["name"]] = h["value"]

    try:
        r = requests.post(url, json=jsondata, headers=pheaders, allow_redirects=True)
    except requests.exceptions.SSLError:
        r = requests.post(url, json=parseddt, headers=pheaders, allow_redirects=True, verify=False)

    algood = False

    for arg in actionarg:
        arg = arg_special_replace(str(arg))
        if action == "or":
            if negate:
                if not arg in r.text:
                    return [ url ]
            else:
                if arg in r.text:
                    return [ url ]
        elif action == "and":
            if negate:
                if not arg in r.text:
                    algood = True
                else:
                    algood = False
            else:
                if arg in r.text:
                    algood = True
                else:
                    algood = False

    if algood:
        return [ url ]

    return []


def doc_contains(url, actionarg, action = "", headers = [], negate = False):
    url = url_special_replace(url)

    if action == "":
        action = "or"

    pheaders = { "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36" }
    for h in headers:
        pheaders[h["name"]] = h["value"]

    try:
        r = requests.get(url, allow_redirects=True, headers=pheaders)
    except requests.exceptions.SSLError:
        r = requests.get(url, allow_redirects=True, verify=False, headers=pheaders)

    algood = False

    for arg in actionarg:
        arg = arg_special_replace(str(arg))
        if action == "or":
            if negate:
                if not arg in r.text:
                    return [ url ]
            else:
                if arg in r.text:
                    return [ url ]
        elif action == "and":
            if negate:
                if not arg in r.text:
                    algood = True
                else:
                    algood = False
            else:
                if arg in r.text:
                    algood = True
                else:
                    algood = False

    if algood:
        return [ url ]

    return []


def is_status_code(url, actionarg, action = "", headers = [], negate = False):
    url = url_special_replace(url)

    if action == "":
        action = "or"

    pheaders = { "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36" }
    for h in headers:
        pheaders[h["name"]] = h["value"]

    try:
        r = requests.head(url, allow_redirects=True, headers=pheaders)
    except requests.exceptions.SSLError:
        r = requests.head(url, allow_redirects=True, verify=False, headers=pheaders)

    algood = False

    for arg in actionarg:
        if action == "or":
            if negate:
                if not int(arg) == r.status_code:
                    return [ url ]
            else:
                if int(arg) == r.status_code:
                    return [ url ]
        elif action == "and":
            if negate:
                if not int(arg) == r.status_code:
                    algood = True
                else:
                    algood = False
            else:
                if int(arg) == r.status_code:
                    algood = True
                else:
                    algood = False

    if algood:
        return [ url ]

    return []


### Custom functions ###


def onet():
    user = args.user

    pheaders = { "Referer": "https://konto.onet.pl/register?client_id=poczta.onet.pl.front.onetapi.pl", "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36" }

    rdomains = requests.get("https://konto.onet.pl/newapi/oauth/domains", headers=pheaders, allow_redirects=True)
    ravdomains =  requests.post("https://konto.onet.pl/newapi/oauth/available-emails", json={"login": str(user)}, headers=pheaders, allow_redirects=True)

    try:
        if rdomains.status_code == 200 and ravdomains.status_code == 200:
            alldoms = json.loads(rdomains.text)["domains"]
            avbdoms = json.loads(ravdomains.text)["emails"]

            for avbdom in avbdoms:
                if avbdom.split("@",1)[1] in alldoms:
                    alldoms.remove(avbdom.split("@",1)[1])

            alldoms = [user + "@" + x for x in alldoms]

            return alldoms

    except:
        return []


def interia():
    user = args.user

    pheaders = { "Referer": "https://konto-pocztowe.interia.pl/",
            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36" }

    s = requests.Session()

    odata = s.get("https://konto-pocztowe.interia.pl/#/nowe-konto/darmowe", headers=pheaders, allow_redirects=True).text

    pheaders["X-App-Fcrtkn"] = odata[(odata.index(",\"X-App-Fcrtkn\":\"")+17):odata.index("\",\"url\":{\"logout\":\"")]

    rdomains = s.get("https://konto-pocztowe.interia.pl/check-login?subdomain=&login=thdabdAWDB12"+''.join(random.choices(string.ascii_uppercase + string.digits, k=12)), headers=pheaders, allow_redirects=True)
    ravdomains = s.get("https://konto-pocztowe.interia.pl/check-login?subdomain=&login="+user, headers=pheaders, allow_redirects=True)

    try:
        rdomains = json.loads(rdomains.text)["data"]["domains"]
        ravdomains = json.loads(ravdomains.text)["data"]["domains"]

        return [el for el in rdomains if el not in ravdomains]

    except:
        return []


def instagram():
    user = args.user

    pheaders = { "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36" }

    csrft = requests.get("https://www.instagram.com/web/__mid/", headers=pheaders, allow_redirects=True).text

    pheaders["X-CSRFToken"] = csrft

    r = requests.post("https://www.instagram.com/accounts/web_create_ajax/attempt/", data={"email":"","username":user,"first_name":"","opt_into_one_tap":"false"}, headers=pheaders, allow_redirects=True)

    if "username_is_taken" in r.text:
        return [ "https://instagram.com/"+user ]

    return []
