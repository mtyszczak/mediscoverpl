# social.json
File containing services (check if username exists) data

This file is universal - you can add predefined action service properties functions (like responsecode, responsecodenot, jsonpostcontainsnot, jsonpostcontains, doccontains and doccontainsnot) without any Python code changes. If you choose custom actions you would have to create new function with the name specified in actionarg in modules.caller

## version
File version in double type

## services
Contains array with all the services available

### services properties
* name - service name (e.g. Onet, Facebook, Twitter etc.)
* url - call (action) url - replace `%%%USERNAME%%%` with given username
* action - call action (what program should do with parsed data)
⋅* responsecode - checks if the reponse code is exactly the given arg
⋅* responsecodenot - checks if the response code is not the given arg
⋅* doccontains - checks if downloaded document contains the given arg
⋅* doccontainsnot - checks if downloaded document does not contain the given arg
⋅* jsoncallcontainsnot - checks if json request does not contain the given arg
⋅* jsoncallcontains - checks if json request contains the given arg
⋅* postdatacontains - checks if post request with data contains the given arg
⋅* postdatacontainsnot - checks if post request with data contains the given arg
⋅* custom - calls actionarg custom function 
* actionarg - action argument

Parsed services properties should always return True (user exists), so if you enter the new, custom website check e.g. if it does not return 404 error (user does not exist) 
