#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# mediscover - banner and help functions module

from mediscover import name, __version__, __author__, __status__, args
from modules.color import color

def startup_banner():
    output = ('{}{}{}{} v{} by {}{}{}'.format(color.ORANGE, color.BOLD, name, color.END, __version__, color.BLUE, __author__, color.END))

    if __status__ == "Development" or args.o_verbose:
        output += ('\n  {}{}[!] {}Development{}'.format(color.RED, color.BOLD, "Forced " if args.o_verbose else "", color.END))

    return output
