#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# https://github.com/r3nt0n/bopscrk
# mediscover - auxiliar functions module

import os, datetime, requests


def clear():
    """Clear the screen. Works on Windows and Linux."""
    os.system(['clear', 'cls'][os.name == 'nt'])

def is_empty(variable):
    """
    Check if a variable (in a type convertible to string) is empty. Returns True or False
    """
    empty = False
    if len(str(variable)) == 0:
        empty = True
    return empty
