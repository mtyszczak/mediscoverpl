#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# LyingCarpet

import os, sys, argparse

from modules.color import color
from modules.auxiliars import is_empty


class Arguments:
    def __init__(self):
        self.DEFAULT_OUTPUT_FILE = 'social.out'

        parser = argparse.ArgumentParser(description='Checks for username in different services')
        group1 = parser.add_mutually_exclusive_group()

        group1.add_argument('-q', '--quiet', action="store_false", dest="o_quiet",
                            help="do not log anything to console")

        group1.add_argument('-v', '--verbose', action="store_true", dest="o_verbose",
                            help="force development debug logging (e.g. exceptions)")

        parser.add_argument('--no-output', action="store_true", dest="o_nooutput",
                            help="log only to console (without saving any data)")

        parser.add_argument('-o', '--output', action="store", type=str,
                            dest='outfile', default=self.DEFAULT_OUTPUT_FILE,
                            help='output file to save (default: {})'.format(self.DEFAULT_OUTPUT_FILE))

        parser.add_argument('-s', '--service', action="store", type=str,
                            dest='services', default='',
                            help='search only in specified services (comma separated)')

        parser.add_argument('username', action="store", type=str,
                            default='',
                            help='username to find')

        self.parser = parser
        self.args = parser.parse_args()
        self.o_quiet = self.args.o_quiet
        self.o_verbose = self.args.o_verbose
        self.o_nooutput = self.args.o_nooutput
        self.outfile = self.args.outfile
        self.user = self.args.username
        self.c_service = self.args.services
