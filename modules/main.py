#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# main.py - script opts and presentation

import sys, os, datetime, json

import traceback

from mediscover import args, __status__
from modules.auxiliars import is_empty
from modules.color import color
from modules.banners import startup_banner
import modules.caller as caller

def run():
    # check Python version
    if sys.version_info < (3, 0): printcl('Python 3 is required'); sys.exit(1)
    # Print simple help and exit when runs without args
    if len(sys.argv) == 1: args.parser.print_help(sys.stdout); sys.exit(2)
    try:

        if args.user == '': args.parser.print_help(sys.stdout); sys.exit(3)

        services = [x.lower() for x in args.c_service.split(",")]

        printcl(startup_banner())

        printcl('\n  {}[+]{} Starting the program...'.format(color.GREEN, color.END))

        # Initial timestamp
        start_time = datetime.datetime.now().time().strftime('%H:%M:%S')

        # Inserting original values into final_wordlist
        base = []

		# Here we go
        fj = open('modules/social.json', 'r')
        outj = json.load(fj)

        printcl('\n  {}[+]{} Loading services v{}...\n'.format(color.GREEN, color.END, outj["version"]))

        for s in outj["services"]:
            if (len(services) == 1 and services[0] == '') or str(s["name"]).lower() in services:
                try:
                    if s["action"] == "responsecode":
                        response = caller.is_status_code(s["url"], s["actionargs"], action=s["action_connect"], headers=s["headers"])
                    if s["action"] == "responsecodenot":
                        response = caller.is_status_code(s["url"], s["actionargs"], action=s["action_connect"], headers=s["headers"], negate=True)
                    if s["action"] == "doccontains":
                        response = caller.doc_contains(s["url"], s["actionargs"], action=s["action_connect"], headers=s["headers"])
                    if s["action"] == "doccontainsnot":
                        response = caller.doc_contains(s["url"], s["actionargs"], action=s["action_connect"], headers=s["headers"], negate=True)
                    if s["action"] == "jsoncallcontains":
                        response = caller.json_call_contains(s["url"], s["actionargs"], requestm=s["request_method"], action=s["action_connect"], headers=s["headers"])
                    if s["action"] == "jsoncallcontainsnot":
                        response = caller.json_call_contains(s["url"], s["actionargs"], requestm=s["request_method"], action=s["action_connect"], headers=s["headers"], negate=True)
                    if s["action"] == "postdatacontains":
                        response = caller.post_data_contains(s["url"], s["actionargs"], action=s["action_connect"], headers=s["headers"])
                    if s["action"] == "postdatacontainsnot":
                        response = caller.post_data_contains(s["url"], s["actionargs"], action=s["action_connect"], headers=s["headers"], negate=True)
                    if s["action"] == "custom":
                        response = getattr(caller, s["actionargs"][0])()
                    if len(response) > 0:
                        if "display_url" in s:
                            response = [ caller.url_special_replace(s["display_url"]) ]
                        response = s["name"] + ": " + ", ".join(str(x) for x in response)
                        printcl('  {}[*]{} {}{}{}'.format(color.BLUE, color.END, color.BOLD, response, color.END))
                        base.append(response)
                    else:
                        response = s["name"] + ": not found"
                        printcl('  {}[*]{} {}{}{}'.format(color.BLUE, color.END, color.GRAY, response, color.END))
                except KeyboardInterrupt:
                    printcl('\n\n  {}[!]{} Exiting... Data not saved!\n'.format(color.RED, color.END))
                    sys.exit(4)
                except Exception:
                    if (__status__ == "Development") and (not args.o_quiet):
                        traceback.print_exc()
                    response = s["name"] + ": Unexpected error occured. Unable to sniff the service. Try again later or update the software"
                    printcl('  {}[!]{} {}'.format(color.RED, color.END, response))


        if not args.o_nooutput:
            # SAVE WORDLIST TO FILE
            ############################################################################
            with open(args.outfile, 'w') as f:
                if len(base) == 0:
                    f.write("No profiles found!\n")
                for word in base:
                    f.write(word + '\n')

        # Final timestamps
        end_time = datetime.datetime.now().time().strftime('%H:%M:%S')
        total_time = (datetime.datetime.strptime(end_time, '%H:%M:%S') -
                      datetime.datetime.strptime(start_time, '%H:%M:%S'))

        # PRINT RESULTS
        ############################################################################
        printcl('\n  {}[+]{} Time elapsed:\t{}'.format(color.GREEN, color.END, total_time))
        printcl('  {}[+]{} Output file:\t{}{}{}{}'.format(color.GREEN, color.END, color.BOLD, color.BLUE, "No output!" if args.o_nooutput else args.outfile, color.END))
        printcl('  {}[+]{} Found social websites:\t{}{}{}\n'.format(color.GREEN, color.END, color.YELLOW, len(base), color.END))
        sys.exit(0)

    except KeyboardInterrupt:
        printcl('\n\n  {}[!]{} Exiting...\n'.format(color.RED, color.END))
        sys.exit(4)

def printcl(content):
    if args.o_quiet:
        print(content)
