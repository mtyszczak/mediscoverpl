[![GPL-3.0 License](https://img.shields.io/badge/license-GPL%203.0-brightgreen.svg)](LICENSE.md)
[![Python 3.8](http://img.shields.io/badge/python-3.8-blue.svg)](https://python.org/downloads)
[![Version 0.1.0](http://img.shields.io/badge/version-0.1.0-orange.svg)](https://gitlab.com/mtyszczak/mediscoverpl/-/commits/master)

# MeDiscover [PL]
Social **Me**dia profiles **Discover**y tool inspired by [sherlock](https://github.com/sherlock-project/sherlock), but including Polish websites

## Requirements
+ **Python 3**
+ *requests*, *argparse*, *datetime* (`python3 -m pip install requirements.txt`)

## Usage
```
usage: mediscover.py [-h] [-q | -v] [--no-output] [-o OUTFILE] [-s SERVICES] username

Checks for username in different services

positional arguments:
  username              username to find

optional arguments:
  -h, --help            show this help message and exit
  -q, --quiet           do not log anything to console
  -v, --verbose         force development debug logging (e.g. exceptions)
  --no-output           log only to console (without saving any data)
  -o OUTFILE, --output OUTFILE
                        output file to save (default: social.out)
  -s SERVICES, --service SERVICES
                        search only in specified services (comma separated)
```
 
## How it works
+ You have to **provide** the **username** which will be searched on various websites (including some email providers)
+ Wait for the program to stop the work :)

### Tips  
+ You can disable console output log using -q switch
+ If something went wrong and you want to report a bug please run the program once again with -v option and paste the output to the issue

### Advanced usage
+ You can add **your** websites in `modules/social.json` file (read the documentation first - `modules/social.json.md`)

## TO-DO list
* POST / GET / PUT / DELETE etc. methods when calling json or simple data
* let users remove unnecessary (blank) fields in `modules/social.json`
* add posibility to analyze every service without given (e.g. with --exclude switch)
* add categories into `modules/social.json`
* add categories into switches in `modules/args.py`
* add more debugging info with the `-v` option
* comment everything
* Pretty print the `modules/social.json.md` file

## Legal disclaimer
This tool is created for the sole purpose of security awareness and education, it should not be used against systems that you do not have permission to test/attack. The author is not responsible for misuse or for any damage that you may cause. You agree that you use this software at your own risk.
